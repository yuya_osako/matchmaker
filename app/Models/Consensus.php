<?php
/**
 * マッチング管理モデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * マッチング管理Modelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Consensus extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

    /**
     * チーム情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams() {
        return $this->belongsTo('App\Team');
    }

    /**
     * スケジュール情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedules() {
        return $this->belongsTo('App\Schedule');
    }
}
