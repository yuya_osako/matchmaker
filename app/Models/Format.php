<?php
/**
 * 活動形式モデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 活動形式Modelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Format extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
}
