<?php
/**
 * 点数モデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 点数Modelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */

class Score extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

    /**
     * チーム情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams() {
        return $this->belongsTo('App\Team');
    }

    /**
     * マッチング管理情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consensuses() {
        return $this->belongsTo('App\Consensus');
    }
}
