<?php
/**
 * カテゴリーモデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * カテゴリーModelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Category extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
}
