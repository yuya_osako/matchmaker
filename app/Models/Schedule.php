<?php
/**
 * スケジュールモデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * スケジュールModelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Schedule extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['date','deleted_at', 'start_time', 'ending_time', 'meeting_time'];

    /**
     * チーム情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams() {
        return $this->belongsTo('App\Team');
    }

    /**
     * 活動形式情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formats() {
        return $this->belongsTo('App\Format');
    }
}
