<?php
/**
 * チャットモデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * チャットModelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Chat extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['sended_at', 'read_at', 'deleted_at'];

    /**
     * チーム情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams() {
        return $this->belongsTo('App\Team');
    }

    /**
     * マッチング管理情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consensuses() {
        return $this->belongsTo('App\Consensus');
    }
}
