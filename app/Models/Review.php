<?php
/**
 * レビューモデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * レビューModelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */
class Review extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

    /**
     * 都道府県情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams() {
        return $this->belongsTo('App\Team');
    }

    /**
     * マッチング管理情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consensuses() {
        return $this->belongsTo('App\Consensus');
    }

    /**
     * 点数情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scores() {
        return $this->belongsTo('App\Score');
    }
}
