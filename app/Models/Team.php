<?php
/**
 * チームモデル
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * チームModelクラス
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 * @package App\Models
 */

class Team extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['last_login', 'deleted_at'];

    /**
     * カテゴリー情報を取得します。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories() {
        return $this->belongsTo('App\Category');
    }
}
