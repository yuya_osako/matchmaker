<?php
/**
 * データベースシーダー
 */

use Illuminate\Database\Seeder;

/**
 * データベースSeederクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategoriesTableSeeder::class,
            TeamsTableSeeder::class,
            FormatsTableSeeder::class,
        ]);
    }
}
