<?php
/**
 * 活動形式シーダー
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * 活動形式Seederクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class FormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('formats');
        $table->delete();
        $list = [
            ['id'=>1, 'format'=>'練習試合'],
            ['id'=>2, 'format'=>'合同練習'],
            ['id'=>3, 'format'=>'練習'],
            ['id'=>4, 'format'=>'ミーティング'],
        ];
        $table->insert($list);
    }
}
