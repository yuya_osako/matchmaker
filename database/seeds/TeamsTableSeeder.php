<?php
/**
 * チームシーダー
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Team;
use App\Models\Category;

/**
 * チームSeederクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->delete();
        $categoryIDs = Category::all()->pluck('id')->all();

        factory(Team::class,20)->create([
           'category_id' => rand(min($categoryIDs), max($categoryIDs)),
        ]);

    }
}
