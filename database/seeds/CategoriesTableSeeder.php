<?php
/**
 * カテゴリーシーダー
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

/**
 * カテゴリーSeederクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('categories');
        $table->delete();
        $list = [
            ['id'=>1, 'category'=>'東京都1部'],
            ['id'=>2, 'category'=>'東京都2部'],
            ['id'=>3, 'category'=>'東京都3部'],
            ['id'=>4, 'category'=>'東京都4部'],
            ['id'=>5, 'category'=>'神奈川県1部'],
            ['id'=>6, 'category'=>'神奈川県2部'],
            ['id'=>7, 'category'=>'神奈川県3部'],
            ['id'=>8, 'category'=>'埼玉県1部'],
            ['id'=>9, 'category'=>'埼玉県2部'],
            ['id'=>10, 'category'=>'埼玉県3部'],
            ['id'=>11, 'category'=>'千葉県1部'],
            ['id'=>12, 'category'=>'千葉県2部'],
            ['id'=>13, 'category'=>'千葉県3部'],
            ['id'=>14, 'category'=>'商社リーグ'],
        ];
        $table->insert($list);
    }
}
