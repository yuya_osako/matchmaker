<?php

/**
 * チームファクトリ
 */

use Faker\Generator as Faker;
use App\Models\Team;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'team' => $faker->company(),
        'last_name' => $faker->lastName(),
        'first_name' => $faker->firstName(),
        'last_kana' => $faker->lastKanaName(),
        'first_kana' => $faker->firstKanaName(),
        'email' =>$faker->email(),
        'phone' => $faker->phoneNumber(),
        'frequency' => $faker->randomDigit(),
        'day' => $faker->randomDigit(),
        'ground' => $faker->word(),
        'max_people' => $faker->randomDigit(),
        'average_people' => $faker->randomDigitNotNull(),
        'hope_category' => $faker->randomDigit(),
        'uniform' => $faker->word(),
        'last_login' => $faker->dateTime(),
    ];
});
