<?php
/**
 * スケジュールマイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * スケジュールMigrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('team_id')->unsigned()->comment('チームID');
            $table->integer('format_id')->unsigned()->comment('フォーマットID');
            $table->date('date')->comment('日程');
            $table->time('start_time')->comment('開始時刻');
            $table->time('ending_time')->comment('終了時刻');
            $table->text('content')->nullable()->comment('集合時刻');
            $table->time('meeting_time')->nullable()->comment('活動内容');
            $table->string('place', 255)->nullable()->comment('活動場所');
            $table->integer('game_time')->unsigned()->nullable()->comment('試合時間');
            $table->integer('game_number')->unsigned()->nullable()->comment('試合本数');
            $table->integer('status')->unsigned()->default(1)->comment('ステータス');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('format_id')->references('id')->on('formats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
