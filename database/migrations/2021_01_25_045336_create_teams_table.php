<?php
/**
 *チームマイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * チームMigrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('category_id')->unsigned()->comment('カテゴリーID');
            $table->string('team', 255)->comment('チーム名');
            $table->string('last_name', 255)->comment('姓');
            $table->string('first_name', 255)->comment('名');
            $table->string('last_kana', 255)->comment('姓かな');
            $table->string('first_kana', 255)->comment('名かな');
            $table->string('email', 255)->comment('メールアドレス');
            $table->string('phone', 255)->comment('電話番号');
            $table->integer('frequency')->unsigned()->nullable()->comment('活動頻度');
            $table->integer('day')->unsigned()->nullable()->comment('曜日');
            $table->string('ground', 255)->comment('獲得可能グラウンド');
            $table->integer('max_people')->unsigned()->nullable()->comment('所属人数');
            $table->integer('average_people')->unsigned()->comment('平均参加人数');
            $table->integer('hope_category')->unsigned()->nullable()->comment('希望対戦カテゴリー');
            $table->string('uniform', 255)->nullable()->comment('ユニフォーム');
            $table->timestamp('last_login')->comment('最終ログイン');
            $table->text('description')->nullable()->comment('自由記述');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
