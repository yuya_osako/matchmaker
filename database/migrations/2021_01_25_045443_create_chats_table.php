<?php
/**
 * チャットマイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * チャットMigrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('team_id')->unsigned()->comment('チームID');
            $table->integer('consensus_id')->unsigned()->comment('コンセンサスID');
            $table->text('chat')->comment('内容');
            $table->timestamp('sended_at')->useCurrent()->comment('送信日時');
            $table->timestamp('read_at')->useCurrent()->comment('既読日時');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('consensus_id')->references('id')->on('consensuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
