<?php
/**
 * 点数マイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 点数Migrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('team_id')->unsigned()->comment('チームID');
            $table->integer('consensus_id')->unsigned()->comment('コンセンサスID');
            $table->integer('score1')->unsigned()->comment('得点');
            $table->integer('score2')->unsigned()->comment('失点');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('consensus_id')->references('id')->on('consensuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
