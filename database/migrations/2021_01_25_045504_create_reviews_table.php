<?php
/**
 * レビューマイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * レビューMigrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('team_id')->unsigned()->comment('チームID');
            $table->integer('consensus_id')->unsigned()->comment('コンセンサスID');
            $table->integer('score_id')->unsigned()->comment('スコアID');
            $table->integer('result')->unsigned()->comment('結果');
            $table->integer('evaluation')->unsigned()->comment('評価');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('consensus_id')->references('id')->on('consensuses');
            $table->foreign('score_id')->references('id')->on('scores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
