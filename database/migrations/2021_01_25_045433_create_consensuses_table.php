<?php
/**
 * マッチング管理マイグレーション
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * マッチング管理Migrationクラスです。
 *
 * @author Yuya Osako <yuyaosako070864@gmail.com>
 */
class CreateConsensusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consensuses', function (Blueprint $table) {
            $table->increments('id')->unsigned()->comment('ID');
            $table->integer('team_id')->unsigned()->comment('チームID');
            $table->integer('schedule_id')->unsigned()->comment('スケジュールID');
            $table->timestamp('created_at')->useCurrent()->comment('作成日時');
            $table->timestamp('updated_at')->useCurrent()->comment('更新日時');
            $table->timestamp('deleted_at')->nullable()->useCurrent()->comment('削除日時');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('schedule_id')->references('id')->on('schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consensuses');
    }
}
